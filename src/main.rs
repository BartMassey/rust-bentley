use std::io::Read;

const BUFSIZ: usize = 64 * 1024;

struct BR549 {
    source: std::fs::File,
    buf: [u8; BUFSIZ],
    posn: usize,
    end: usize,
}

impl BR549 {
    fn new(source: std::fs::File) -> Self {
        Self {
            source,
            buf: [0u8; BUFSIZ],
            posn: 0,
            end: 0,
        }
    }
}

impl Iterator for BR549 {
    type Item = u8;

    #[inline(always)]
    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if self.posn < self.end {
                // Need: This shaves about 0.6s off a 1GB
                // iteration of about 2.7s on my box.
                // Safety: `self.end` can never be larger
                // than `BUFSIZ`, so `self.posn` can never
                // point past the end of `self.buf`.
                let b = unsafe {
                    *self.buf.as_ptr().offset(self.posn as isize)
                };
                self.posn += 1;
                return Some(b);
            }
            if self.end == 0 || self.end == BUFSIZ {
                self.end = self.source.read(&mut self.buf).unwrap();
                self.posn = 0;
                continue;
            }
            return None;
        }
    }
}

fn main() -> std::io::Result<()> {
    let args: Vec<String> = std::env::args().collect();
    let file = &args[1];
    let _k: usize = args[2].parse().unwrap();

    let file = std::fs::File::open(file)?;
    let text = BR549::new(file);

    let mut word_buf = [0u8; 128];
    let mut nword = 0;
    // `true` for collecting letters; `false` for skipping non-letters.
    let mut mode = true;
    let mut count = 0;
    for c in text {
        let c = c | 0x20;
        let letter = c >= b'a' && c <= b'z';
        if letter {
            // Need: This shaves about 0.35s off a 1GB
            // iteration of about 3.1s on my box.
            // XXX Lack of Safety!: Definitely UB if a word
            // larger than 128 bytes is encountered.
            unsafe { *word_buf.as_mut_ptr().offset(nword) = c };
            nword += 1;
            mode = true;
        } else if mode {
            count += 1;
            nword = 0;
            mode = false;
        }
    }
    if mode && !word_buf.is_empty() {
        count += 1;
    }

    println!("{}", count);
    Ok(())
}
